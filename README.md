# Quiz visuals
A simple quiz app mock made as an exercise in QML.


## Contributors
Eivind Vold Aunebakk

## Screenshots
![screenshot](screenshot.png)