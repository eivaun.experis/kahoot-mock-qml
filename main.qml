import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.1

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Quiz")

    property string quiz_player: "Eivind"
    property int quiz_score: 1234
    property string quiz_title: "Animals"
    property string quiz_question: "What sort of creature is a dingo?"
    property var quiz_answers: ["A dinosaur", "A fish", "A wild dog", "A wild cat", "A bird", "An insect"]
    property var quiz_button_colors: ["#FF6D6A", "#8BD3E6", "#77dd77", "#E9EC6B", "#EFBE7D", "#B1A2CA"]


    ColumnLayout
    {
        anchors.centerIn: parent
        height: parent.height-20
        width: parent.width-20
        Label
        {
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            text:quiz_title
            font.pixelSize: 32
        }
        Label
        {
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            text:quiz_question
            font.pixelSize: 20
        }
        Grid
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            columns:2
            rows: Math.ceil(quiz_answers.length/2)
            spacing:10
            Repeater
            {
                model: quiz_answers.length
                AbstractButton {
                    width: (parent.width - parent.spacing*(parent.columns-1)) / parent.columns
                    height: (parent.height - parent.spacing*(parent.rows-1)) / parent.rows

                    contentItem: Text {
                        text: quiz_answers[index]
                        font.pixelSize: 20
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }

                    background: Rectangle {
                        border.width: 0
                        radius: 10
                        property color colorNormal: quiz_button_colors[index%6]
                        property color colorHovered: Qt.lighter(colorNormal, 1.1)
                        property color colorPressed: Qt.lighter(colorNormal, 1.2)
                        color: parent.pressed ? colorPressed : ( parent.hovered ? colorHovered : colorNormal)
                    }
                }
            }
        }
        RowLayout
        {
            Layout.fillWidth: true
            Label
            {
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignLeft
                text:quiz_player
                font.pixelSize: 20
            }
            Label
            {
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignRight
                text:quiz_score
                font.pixelSize: 20
            }
        }
    }
}



